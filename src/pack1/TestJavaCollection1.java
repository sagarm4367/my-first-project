package pack1;

import java.util.ArrayList;
import java.util.Iterator;

public class TestJavaCollection1 {
	
	public static void main(String args[]) {
		
		
		ArrayList<String> list1= new ArrayList<String>();
		
		list1.add("sagar");
		list1.add("arif");
		list1.add("vishnu");
		list1.add("reshma");
		
		
		
		System.out.println(list1);
		
		Iterator itr=list1.iterator();
		System.out.println("printing all elements");
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		
        list1.remove(1);

	}

}
