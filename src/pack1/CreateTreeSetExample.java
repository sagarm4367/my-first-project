package pack1;

import java.util.SortedSet;
import java.util.TreeSet;

public class CreateTreeSetExample {
    public static void main(String[] args) {
        // Creating a TreeSet
        SortedSet<String> fruits = new TreeSet<>();
        
        SortedSet<String> fruits1 = new TreeSet<>();

        // Adding new elements to a TreeSet
        fruits.add("Banana");
        fruits.add("Apple");
        fruits.add("Pineapple");
        fruits.add("Orange");
        
        System.out.println("Fruits Set : " + fruits);
        System.out.println("**************************************");

        fruits1.add("Banana");
        fruits1.add("Apple");
        fruits1.add("Pineapple");
        fruits1.add("Orange");
        fruits1.remove("Orange");

        
        System.out.println("Fruits1 Set : " + fruits1);
        
        System.out.println("**************************************");

        // Duplicate elements are ignored
        fruits.add("Apple");
        System.out.println("After adding duplicate element \"Apple\" : " + fruits);

        // This will be allowed because it's in lowercase.
        fruits.add("banana");
        System.out.println("After adding \"banana\" : " + fruits);
 
        
        System.out.println(fruits.first());
        
        boolean b=fruits.contains("Orange");
        System.out.println(b);
        
        boolean b1=fruits.containsAll(fruits1);
        System.out.println(b1);
        
        
        fruits.clear();
        System.out.println("After adding \"banana\" : " + fruits); 
               
               
        }
}
